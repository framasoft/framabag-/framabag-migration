#!/usr/bin/env python3

import os
import json
import csv


def importentries(id_user, user):
    filename = "json/" + user + ".json"
    print("Trying to import file " + filename)
    os.system("wallabag/bin/console wallabag:import " + str(id_user) + " " + filename + " --env=prod")

print("Getting list of users")

errors = []
with open('accounts.csv', newline='') as csvfile:
    next(csvfile)
    reader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in reader:
        if os.path.isfile("json/" + row[1] + ".json"):
            importentries(row[0], row[1])
        else:
            print("no json file exists for this user.")
            errors.append(row[1])


with open('errors-import.json', 'w') as fp:
    json.dump(errors, fp)
